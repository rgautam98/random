var express = require('express');
var router = express.Router();
const { MongoClient } = require('mongodb')


const url = process.env.MONGO_URL || 'mongodb://localhost:27017'

console.log("using url ")
console.log(url)
const client = new MongoClient(url)

// Database Name
const dbName = 'foo'

async function main() {
  // Use connect method to connect to the server
  await client.connect()
  console.log('Connected successfully to server')
  const db = client.db(dbName)
  const collection = db.collection('documents')
  const insertResult = await collection.insertMany([{ a: 1 }, { a: 2 }, { a: 3 }])
  console.log('Inserted documents =>', insertResult)
  return 'done.'
}




/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});


router.get("/healthz", function(req, res){
  // console.log()
  res.status(200).send("OK")
})

router.get("/mongo/test", async function(req, res){
  console.log("Getting the mongodb data")
  await main()
  res.status(200).send("Mongodb Data Got")
})

module.exports = router;
